import os, re

def get_event_count(filename):
	"""Returns the number of lines in a file as an int"""
	event_count = 0
	with open(filename) as f:
		for line in f:
			if line.strip():
				event_count += 1
	return event_count

def test_event_count():
	""" 
	Verifies the total event count from both targets.
	Expected count is 1,000,000.
	"""

	input_event_count = 1000000

    # get the number of events in teh output file
	output_event_count = 0
	output_event_count = get_event_count('logs/smoke/events.log')
	output_event_count += get_event_count('logs/smoke/events2.log')
	assert input_event_count == output_event_count

def test_event_correctness():
	'''
	Verify all of the events are correct by matching them to the event format
	in the input file.
	'''

	p = re.compile(r'This is event number \d+\s')

	with open('logs/smoke/events.log') as log1:
		for line in log1:
			assert p.match(line) != None

	with open('logs/smoke/events2.log') as log1:
		for line in log1:
			assert p.match(line) != None

def test_duplicate_events():
	""" 
	Verifies there are no duplicate events.
	"""
	event_dictionary = {}
	event_dictionary_two = {}

	with open('logs/smoke/events.log') as log1:
		for line in log1:
			# verify the event from is not already in the dictionary
			assert not line in event_dictionary
			event_dictionary[line] = True

	with open('logs/smoke/events2.log') as log2:
		for line in log2:
			# verify the event is not in the previous log file.
			assert not line in event_dictionary
			# verify the event is not in the current dictionary
			assert not line in event_dictionary_two
			event_dictionary_two[line] = True

def test_spitter_balance():
	"""
	Tests the distribution of events by the splitter.  The ideal ratio of
	event distribution is even.  The test verify the ratio is "close" to the
	ideal ratio of 1. No target should have less than 91% of the events the
	other has.
	"""
	output_one = get_event_count('logs/smoke/events.log')
	output_two = get_event_count('logs/smoke/events2.log')

	split_ratio = float(output_one)/float(output_two)

	assert abs(1 - split_ratio) < .09

def test_zero_size_input():
	""" 
	Verifies system behavior when an empty file is processed by the agent.
	It is expected this will not result in an error and no even logs produced
	by the targets.
	"""
	with open('logs/no_events_container_logs.txt') as no_events_log:
		for line in no_events_log:
			assert "Error" not in line

	cwd = os.getcwd()
	assert os.path.isfile(
		os.path.join(cwd,'logs/no_events/events.log')) != True
	assert os.path.isfile(
		os.path.join(cwd,'logs/no_events/events2.log')) != True

def test_10x_agent_run():
	""" 
	Verifies event counts for success runs of the agent.  The agent container
	is run 10 times consecutively.
	"""
	event_count = get_event_count('logs/multiple_agent_run/events.log')
	event_count += get_event_count('logs/multiple_agent_run/events2.log')

	assert event_count == 10*1000000

def test_10x_agent_errors():
	"""Verifies no errors occur when agent is run mulitple times"""
	with open('logs/multiple_agent_run_logs.txt') as no_events_log:
		for line in no_events_log:
			assert "Error" not in line

