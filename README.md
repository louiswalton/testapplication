# testapplication

## Run Test
+ run the command `./run_test`

## Requirements
+ The test use `docker-compose` and need a docker environment to run.
+ Docker version 20.10.7.
+ The `run_test` script assumes a bash prompt
+ The directory name the repository is cloned into is important and must be named `testapplication`.  The docker network uses the directory name as a prefix and the tests will not run if this is not correct.
+ Artifacts from each run for all services are put into the logs-MM-DD-YY-Seconds directory.


## Approach
**Building the environment** - I used `docker-compose` to build the stack in the correct order with a dockerfile for each service.

**Tests** - The logs for each test are stored in a different folder in the `logs` directry while the tests are running.  I have chosen to run the tests in a container because they can also be run locally.  To avoid conflicts with python versions and the differences in environments it is easier to run the test in a container for consistency in this case.  The agent, splitter, and target do not produce any logs to show when they complete the processing of events.  So it can be difficult to know when the events have passed through the stack in a deterministic way.  To avoid any timing issues, I poll to see when the agent container has exited.  I then try to copy the events.log from the target service for 10 seconds.  From observations it is unlikely to take more than a second for the events.log file to appear and be copied.

+ Smoke - After the agent service has completed sending events, the event count is verified, the event logs are checked for duplicate events, each event is checked for correctness, and the event logs from each target are compared to see if the splitter is splitting the events in a balanced way.

+ Empty Source - This test uses a source file that is empty with no events.  The tests checks for any error in the service logs to see if the empty file caused an error in any service.  It also verifies no events were produced by the target.  

+ Agent run multiple times - This test runs the agent service 10 times and verifies no errors appear in the logs and the event count is correct.

**Findings**
1. Line breaking is inconsistent and the event correctness test rarely passes.  The event logs have many lines that look like `This is event number`.  Surprisingly, the event count is rarely wrong even when the event logs have this issue.  
2. The error in line breaking can sometimes be the cause of flakiness in the duplicate event test. As an example, the line `'This is event number 25\n'` appeared in one of the even logs twice because of a line breaking error.  The line breaking test failed because the sample line could have actually have any number that fit the patter of 25x, 25xx, 25xxx, 25x,xxx and the error in line breaking split it after the digits two and 5.
3. The splitter crashes if one of the targets goes down.  It would be nice if the splitter continued to send data to the remaining targets that are still running.


## CICD
+ It seems that artifacts are not stored by the actions if the action exits with a non-zero code.  The test usually have one failure because of event correctness and this was causing the artifacts to not be uploaded.  Since keeping the artifacts is a requirement, the test script finishes with a command that will exit with a 0 so artifacts will be stored.
