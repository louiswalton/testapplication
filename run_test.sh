#!/bin/bash

function wait_container_finish() {
    echo -n "Waiting for agent to finish "
    while [[ $(docker container ls -a -f status=exited --format '{{.State}}' --filter name=$1) != "exited" ]]
    do
        echo -n "."
        sleep 1
    done 
    echo " Completed!"
}

function download_container_logs() {
    echo -n "Downloading logs from $1 for $2."
    end=$((SECONDS+3))
    while [[ $SECONDS -le $end ]]
    do
        docker cp $1:/home/node/app/events.log $2 2>/dev/null
        if [[ $? == 0 ]]
        then
            break
        fi
        sleep 1
        echo -n "."
    done
    echo " Completed!"
}

################################
## Configure env for smoke tests
################################

mkdir logs
#docker system prune -f
docker-compose rm -f
docker-compose up --build -d target_1 target_2 splitter agent &

wait_container_finish agent

#get container logs
docker-compose logs -f > logs/smoke_container_logs.txt &

mkdir logs/smoke

download_container_logs testapplication_target_1_1 logs/smoke/events.log
download_container_logs testapplication_target_2_1 logs/smoke/events2.log

############################################
## Configure env for mulitple agent run test
############################################

mkdir logs/multiple_agent_run

#remove the old agent container
docker-compose rm -f

# delete the old event logs before the next test
docker exec testapplication_target_1_1 rm /home/node/app/events.log
docker exec testapplication_target_2_1 rm /home/node/app/events.log

docker-compose run agent 2>/dev/null
wait_container_finish agent
# remove the old agent container
docker-compose rm -f

for i in {1..9}
do
    docker-compose run agent 2>/dev/null
    wait_container_finish agent
    docker-compose rm -f
done
docker-compose logs -f > logs/multiple_agent_run_logs.txt &
sleep 2

download_container_logs testapplication_target_1_1 logs/multiple_agent_run/events.log
download_container_logs testapplication_target_2_1 logs/multiple_agent_run/events2.log

###########################################
## Configure env for empty source file test
###########################################

mv agent/inputs/large_1M_events.log agent/inputs/large_1M_events.old
touch agent/inputs/large_1M_events.log

# delete the old event logs before the next test
docker exec testapplication_target_1_1 rm /home/node/app/events.log
docker exec testapplication_target_2_1 rm /home/node/app/events.log

# run the agent with empty input file
docker-compose run agent_no_events node app.js .

#get container logs
docker-compose logs -f > logs/no_events_container_logs.txt &

mkdir logs/no_events

download_container_logs testapplication_target_1_1 logs/no_events/events.log
download_container_logs testapplication_target_2_1 logs/no_events/events2.log
ls logs/no_events

docker-compose kill

rm agent/inputs/large_1M_events.log
mv agent/inputs/large_1M_events.old agent/inputs/large_1M_events.log

# docker build . -t tests -f Dockerfile-test
# docker run tests
pytest -v

cp -r logs/ "logs-$(date +"%m-%d-%y-%r")"